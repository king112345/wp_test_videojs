<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test_video' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[*t{_Otg>d9a?,UYT1mT$I<NIwC^+)lDpga*dIp^-oK$W(x:Kxkq,?O206THlGx{' );
define( 'SECURE_AUTH_KEY',  ').LEhS(wRP_PS/)b8I}2R^+(lx9k5-&mZ$J.}@zG#_6>sG^Xa-z T}V]kTs5L*O ' );
define( 'LOGGED_IN_KEY',    'n9~~n|f3^!>$GEV*#1R`%3Jre<%3(J`}&|U7{n_=APclm-#7Va]e$X q%Svg!OiW' );
define( 'NONCE_KEY',        '5mx2)KwVE[+ I<f(b6Tt=$SH<D2mOLh_);!9Jq|j@Ww~R=_/[=$qvMlw{<^hER w' );
define( 'AUTH_SALT',        '6785r.x,nse>YukSmVq{lAI<*u3pjzf>Zo!SyDJdkQ}.In;!M1%0l](GQQyF]!3b' );
define( 'SECURE_AUTH_SALT', 'F1t1XDC-vU3jTSN%Rv(:,!jFbmQ1;p_bL3MqHzd3Y}*y#B^dd-m,kGG@6}/)aCHp' );
define( 'LOGGED_IN_SALT',   'OZH+;y^}-g>YjL/>Z[0gSJy+W4e_`c$9hL{*7p~dl&HN%S_r#&m}E4N_]{+iQ.#R' );
define( 'NONCE_SALT',       'f!uaK49ohRC#PWl?fnZyFjXyZl0Ll,s@]Vr|#uaj^D$k_K}H3a/5=Q_56^:%^I(d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
